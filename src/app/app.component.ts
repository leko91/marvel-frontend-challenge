import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { SearchService } from './search.service';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [SearchService]
})

export class AppComponent {
  title = 'Marvel Frontend Challenge';
  private apiUrl = 'https://gateway.marvel.com:443/v1/public/characters?limit=12&apikey=e07bc9715ac6e7ae5ab7b133cc068372';
  results: Object;
  searchTerm$ = new Subject<string>();
  bookmarked: Object[] = [];
  searchResults: Object[];

  constructor (private http: Http, private searchService: SearchService) {
    this.searchService.search(this.searchTerm$)
      .subscribe(subscribeResults => {
        this.searchResults = subscribeResults.data.results;
      });
  }

  addBookmark (clicked) {
    this.bookmarked.push(clicked);
  }
}
