import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Injectable()
export class SearchService {
  baseUrl: string = 'https://gateway.marvel.com:443/v1/public/characters'
  queryUrl: string = 'limit=12&apikey=e07bc9715ac6e7ae5ab7b133cc068372';
  searchUrl: string = '?nameStartsWith=';

  constructor(private http: Http) { }

  search(terms: Observable<string>) {
    return terms.debounceTime(400)
      .distinctUntilChanged()
      .switchMap(term => this.searchEntries(term));
  }

  searchEntries(term) {
    // At least show something when it's empty
    if (term === '') {
      this.searchUrl = '?';
    } else {
       this.searchUrl = '?nameStartsWith=';
    }

    return this.http
      .get(this.baseUrl + this.searchUrl + term + '&' + this.queryUrl)
      .map(res => res.json());
  }
}